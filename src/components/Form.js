import React, { Component } from "react"

class Form extends Component {
  state = {
    login: "",
    password: ""
  }

  handleChange = (e, type) => {
    this.setState({
      [type]: e.target.value
    })
  }

  handleSubmit = e => {
    e.preventDefault()
    console.log("Submitted")
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>{`My login is ${this.state.login}, my password is ${this.state.password}`}</label>
        <input
          type="text"
          value={this.state.login}
          placeholder="Login"
          onChange={event => this.handleChange(event, "login")}
        ></input>

        <input
          type="password"
          value={this.state.password}
          placeholder="Password"
          onChange={e => this.handleChange(e, "password")}
        ></input>
        <button type="submit">Send</button>
      </form>
    )
  }
}

export default Form
