import React from "react"

const List = ({ name, age, items }) => (
  <>
    <ul>
      {items.map((item, index) => (
        <li key={index.toString()}>{item}</li>
      ))}
    </ul>
    <p>{`My name is ${name}, I am ${age} years old`}</p>
  </>
)

export default List
