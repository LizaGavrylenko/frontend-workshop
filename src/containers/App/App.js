import React, { useState, useEffect } from "react"
import { connect } from "react-redux"

import "../../App.css"
import List from "../../components/List"
import Form from "../../components/Form"

import { getItems } from "./AppActions"

const mapStateToProps = (state, props) => ({
  items: state.app.items,
  error: state.app.error
})

const mapDispatchToProps = { getItems }

function App({ getItems, items, error }) {
  const [name, setName] = useState("Liza")
  useEffect(() => {
    getItems([1, 2, 3])
  }, [])

  const handleClick = () => {
    setName("Other")
  }

  return (
    <div className="App">
      <header className="App-header">
        <div>{`Hello ${name}`}</div>
        <List name={name} age="24" items={items} />
        <button onClick={handleClick}>Change name</button>
      </header>
      <Form />
      {error && <p style={{ color: "red" }}>{error}</p>}
    </div>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
