import {
  GET_ITEMS,
  GET_ITEMS_FULFILLED,
  GET_ITEMS_REJECTED
} from "./AppActions"

export const initialState = {
  items: [],
  error: null
}

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ITEMS_FULFILLED: {
      return {
        ...state,
        items: [...state.items, ...action.payload]
      }
    }
    case GET_ITEMS_REJECTED: {
      return {
        ...state,
        error: action.payload
      }
    }
    default:
      return state
  }
}

export default appReducer
