import * as api from "../../api"

export const GET_ITEMS = "GET_ITEMS"
export const GET_ITEMS_FULFILLED = "GET_ITEMS_FULFILLED"
export const GET_ITEMS_REJECTED = "GET_ITEMS_REJECTED"

export const getItems = ids => ({
  type: GET_ITEMS,
  payload: api.getItems(ids).then(items => items)
})
