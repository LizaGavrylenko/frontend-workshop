export const getItems = ids =>
  new Promise((resolve, reject) => {
    ids.forEach(id => {
      if (typeof id === "string") {
        reject("You supplied wrong item id. Getting items failed.")
      }
    })

    resolve(["Sleep", "Do yoga", "Work", "Run", "Travel"])
  })
