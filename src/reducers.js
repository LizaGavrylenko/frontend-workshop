import { combineReducers } from "redux"
import app from "./containers/App/AppReducer"

export default () =>
  combineReducers({
    app
  })
