import { createStore, compose, applyMiddleware } from "redux"
import promise from "redux-promise-middleware"
import thunk from "redux-thunk"
import rootReducer from "../reducers"

function configureStore(initialState) {
  let store = null

  store = createStore(
    rootReducer(),
    initialState,
    compose(
      applyMiddleware(promise, thunk),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  )

  return store
}

export default configureStore
